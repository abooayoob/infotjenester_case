import React from 'react'
import { render } from 'react-dom'
import Branding from './Branding'
import Content from './Content'
import WaitForIt from './WaitForIt'
import '../sass/main.sass'

const App = React.createClass({
  getInitialState () {
    return {
      dataFromServer: false,
      searchTerm: '',
      filters: [],
      showFilters: false
    }
  },
  componentDidMount () {
    window.fetch('http://localhost:3000/api')
     .then(response => response.json())
     .then(data => {
       this.setState({ dataFromServer: data })
     })
  },
  handleSearchTermChange (event) {
    this.setState({searchTerm: event.target.value})
  },
  addFilter (filter) {
    this.setState({ filters: this.state.filters.concat([filter]) })
  },
  removeFilter (filter) {
    let { filters } = this.state
    const index = filters.indexOf(filter)
    filters.splice(index, 1)
    this.setState({ filters })
  },
  toggleFilterArea () {
    if (this.state.showFilters) {
      this.setState({ filters: [] })
    }
    this.setState({ showFilters: !this.state.showFilters })
  },
  render () {
    return (
      <div className='container'>

        <div className='header'>
          <Branding />
        </div>

        {
          // WARNING:  this whole block is a ternary, yikes!
          this.state.dataFromServer
          ? <Content
            showFilters={this.state.showFilters}
            addFilter={this.addFilter}
            removeFilter={this.removeFilter}
            toggleFilterArea={this.toggleFilterArea}
            filters={this.state.filters}
            handleSearchTermChange={this.handleSearchTermChange}
            articleData={this.state.dataFromServer}
            searchTerm={this.state.searchTerm}
            />
          : <WaitForIt />
        }

        <div className='footer'>
          <a href=''>
            <div className='good-bye'>
              Hjem
            </div>
          </a>
        </div>
      </div>
    )
  }
})

render(<App />, document.getElementById('app'))
