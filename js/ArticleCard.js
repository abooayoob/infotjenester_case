import React from 'react'
const { string } = React.PropTypes

const ArticleCard = React.createClass({
  propTypes: {
    articletitle: string,
    articledate: string,
    articleingress: string,
    articletag: string,
    articleurl: string,
    articleimage: string
  },
  render () {
    const {
      articletitle,
      articledate,
      articleingress,
      articletag,
      articleurl,
      articleimage
      } = this.props

    // Format date properly from yyyy-mm-ddThh:mm:ss to dd-mm-yyyy
    const tmp = articledate.split('T')
    const date = tmp[0].split('-').reverse().join('-')

    return (
      <div className='article-card'>
        <a href={articleurl}>
          <div className='article-card__header'>
            <img src={articleimage} alt={`illustrasjonsbilde for artikkel om ${articletitle}`} className='article-card__image' />
            <div className='article-card__date'>{date}</div>
            <h2 className='article-card__title'>{articletitle}</h2>
            <div className='article-card__tag'>{articletag}</div>
          </div>
          <div className='article-card__ingress'>
            {articleingress}
            <span className='arrow'>→</span>
          </div>
        </a>
      </div>
    )
  }
})

export default ArticleCard
