import React from 'react'

const Branding = React.createClass({
  render () {
    return (
      <div className='header__branding'>
        <img
          src='https://www.infotjenester.no/images/logos/logo-header-liten.png'
          className='header__logo'
        />
        <h1 className='header__main-heading'>Artikler fra Infotjenester</h1>
      </div>
    )
  }
})

export default Branding
