import React from 'react'
import ArticleCard from './ArticleCard'
import SearchArea from './SearchArea'
const { array, string, func, bool } = React.PropTypes

const Content = React.createClass({
  propTypes: {
    articleData: array,
    filters: array,
    searchTerm: string,
    handleSearchTermChange: func,
    toggleFilterArea: func,
    addFilter: func,
    removeFilter: func,
    showFilters: bool
  },
  render () {
    return (
      <div className='content'>
        <SearchArea
          addFilter={this.props.addFilter}
          removeFilter={this.props.removeFilter}
          showFilters={this.props.showFilters}
          toggleFilterArea={this.props.toggleFilterArea}
          handleSearchTermChange={this.props.handleSearchTermChange}
          articleData={this.props.articleData}
        />

        <div className='content__articles'>
          {
            this.props.articleData
            .filter(article => {
              if (!this.props.filters.length) {
                return true
              } else {
                return this.props.filters.includes(article.articletag)
              }
            })
            .filter(article => {
              return (
                `${article.articletag} ${article.articletitle} ${article.articleingress}`
                .toUpperCase().includes(this.props.searchTerm.toUpperCase())
              )
            })
            .map(article => {
              return (
                <ArticleCard key={article.articleid} {...article} />
              )
            })
          }
        </div>
      </div>
    )
  }
})

export default Content
