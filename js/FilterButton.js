import React from 'react'
const { string, func } = React.PropTypes

const FilterButton = React.createClass({
  getInitialState () {
    return {
      active: false
    }
  },
  propTypes: {
    buttonName: string,
    addFilter: func,
    removeFilter: func
  },
  handleClick () {
    if (!this.state.active) {
      this.props.addFilter(this.props.buttonName)
      this.setState({ active: true })
    } else {
      this.props.removeFilter(this.props.buttonName)
      this.setState({ active: false })
    }
  },
  render () {
    const yeah = {
      color: 'white',
      backgroundColor: '#E91E63'
    }
    return (
      <button
        style={this.state.active ? yeah : null}
        onClick={this.handleClick}
        className='filter-area__button'>{this.props.buttonName}
      </button>
    )
  }
})

export default FilterButton
