import React from 'react'
import FilterButton from './FilterButton'
const { func, bool, array } = React.PropTypes

const SearchArea = React.createClass({
  propTypes: {
    handleSearchTermChange: func,
    toggleFilterArea: func,
    addFilter: func,
    removeFilter: func,
    showFilters: bool,
    articleData: array
  },
  render () {
    // The following 6 lines is just one nonsexy way of getting an array of uniqe tags
    const tagArray = this.props.articleData.map(article => article.articletag)
    const uniqeTagsSet = new Set(tagArray)
    const uniqeTagsArray = []
    uniqeTagsSet.forEach(tag => {
      uniqeTagsArray.push(tag)
    })

    let id = 1
    return (
      <div className='content__search'>
        <div className='search-area'>
          <button onClick={this.props.toggleFilterArea} className='filter'>Filtrer</button>
          <input
            type='text'
            placeholder='Søk'
            className='search-input'
            onChange={this.props.handleSearchTermChange}
          />
        </div>

        {
          // WARNING:  this whole block is a ternary, yikes!
          this.props.showFilters
          ? <div className='filter-area'>
            {
              uniqeTagsArray
              .map(tag => {
                return (
                  <FilterButton
                    addFilter={this.props.addFilter}
                    removeFilter={this.props.removeFilter}
                    key={id++} buttonName={tag}
                  />
                )
              })
            }

            <button
              className='filter-area__button--remove'
              onClick={this.props.toggleFilterArea}
            >
              Fjern filtre
            </button>
          </div>
          : null
        }

      </div>

    )
  }
})

export default SearchArea
